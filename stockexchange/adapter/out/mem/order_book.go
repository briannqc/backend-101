package mem

import (
	"context"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type OrderBookAdapter struct {
	ob *domain.OrderBook
}

func NewOrderBookAdapter() *OrderBookAdapter {
	return &OrderBookAdapter{ob: domain.NewOrderBook()}
}

func (a *OrderBookAdapter) GetOrderBook(ctx context.Context) (*domain.OrderBook, error) {
	return a.ob, nil
}
