package sqlite

import (
	"context"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type Order struct {
	ID        string `db:"id"`
	Symbol    string `db:"symbol"`
	Quantity  uint   `db:"quantity"`
	PriceCent uint64 `db:"price_cent"`
	Side      string `db:"side"`
	Status    string `db:"status"`
}

type OrderPersistenceAdapter struct {
	db *sqlx.DB
}

func NewOrderPersistenceAdapter(db *sqlx.DB) *OrderPersistenceAdapter {
	return &OrderPersistenceAdapter{db: db}
}

func (a *OrderPersistenceAdapter) InsertOrder(ctx context.Context, order domain.Order) error {
	tx, err := a.db.Beginx()
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO stock_order(id, symbol, quantity, price_cent, side, status) VALUES(?, ?, ?, ?, ?, ?);`,
		order.ID(),
		string(order.Symbol()),
		order.Quantity(),
		uint64(order.Price()),
		string(order.Side()),
		string(order.Status()),
	)
	if err != nil {
		return err
	}
	return tx.Commit()
}

func (a *OrderPersistenceAdapter) UpdateAllOrders(ctx context.Context, orders ...domain.Order) error {
	tx, err := a.db.Beginx()
	if err != nil {
		return err
	}
	for _, order := range orders {
		_, err = tx.ExecContext(
			ctx,
			`UPDATE stock_order SET symbol=?, quantity=?, price_cent=?, side=?, status=? WHERE id=?;`,
			string(order.Symbol()),
			order.Quantity(),
			uint64(order.Price()),
			string(order.Side()),
			string(order.Status()),
			order.ID(),
		)
		if err != nil {
			return err
		}
	}
	return tx.Commit()
}

func (a *OrderPersistenceAdapter) GetOrderByID(
	ctx context.Context,
	id string,
) (domain.Order, error) {
	var dbOrder Order
	if err := a.db.GetContext(ctx, &dbOrder, "SELECT * FROM stock_order WHERE id=?", id); err != nil {
		return domain.Order{}, err
	}
	order, err := domain.NewOrder(
		dbOrder.ID,
		dbOrder.Symbol,
		dbOrder.Quantity,
		dbOrder.PriceCent,
		dbOrder.Side,
		dbOrder.Status,
	)
	if err != nil {
		log.Printf("Failed to reconstruct Order from DB record, it might be corrupted, id=%s", id)
		return domain.Order{}, err
	}
	return order, nil
}
