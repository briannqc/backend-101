package web

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

func newSuccessfulResponse(message string, data interface{}) gin.H {
	return gin.H{
		"message": message,
		"data":    data,
	}
}

func newErrorResponse(errorCode, message, details string) gin.H {
	return gin.H{
		"errorCode": errorCode,
		"message":   message,
		"details":   details,
	}
}

func errorResponseOf(err error) gin.H {
	if domainErr, ok := err.(domain.Error); ok {
		return gin.H{
			"errorCode": domainErr.Code(),
			"message":   domainErr.Message(),
			"details":   domainErr.Details(),
		}
	}

	log.Println("Uncategorized error happened, returning default error", err)
	defaultNonSensitiveErrorResponse := gin.H{
		"errorCode": "ERR0000",
		"message":   "Something wrong happened at our end",
		"details":   "Please try again after awhile. If the issue keeps happening, contact our Customer Support.",
	}
	return defaultNonSensitiveErrorResponse
}

func statusCodeOf(err error) int {
	domainErr, ok := err.(domain.Error)
	if !ok {
		return http.StatusInternalServerError
	}

	switch domainErr.Category() {
	case domain.InvalidInput:
		return http.StatusBadRequest
	case domain.Unauthorized:
		return http.StatusForbidden
	default:
		return http.StatusInternalServerError
	}
}
