package web_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/adapter/in/web"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain/stub"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/in"
)

func TestGetOrderHandler_HandleGetOrder(t *testing.T) {
	type UCReturn struct {
		order domain.Order
		err   error
	}
	tests := []struct {
		name             string
		id               string
		ucReturn         UCReturn
		wantStatusCode   int
		wantResponseBody string
	}{
		{
			name: "GIVEN unauthorized THEN return 403",
			id:   "order-01",
			ucReturn: UCReturn{
				err: domain.ErrCannotViewOrder,
			},
			wantStatusCode: http.StatusForbidden,
			wantResponseBody: fmt.Sprintf(
				`{"errorCode": "%v", "message": "%v", "details": "%v"}`,
				domain.ErrCannotViewOrder.Code(),
				domain.ErrCannotViewOrder.Message(),
				domain.ErrCannotViewOrder.Details(),
			),
		},
		{
			name: "GIVEN authorized THEN return 200 along with order details",
			id:   "order-02",
			ucReturn: UCReturn{
				order: stub.BuyOrderNew(),
			},
			wantStatusCode: http.StatusOK,
			wantResponseBody: fmt.Sprintf(
				`{"message": "Get Order details successfully", "data": %s}`, stub.BuyOrderNewJSON(),
			),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			uc := in.NewMockGetOrderUseCase(ctrl)
			uc.EXPECT().
				GetOrder(gomock.Any(), tt.id).
				Return(tt.ucReturn.order, tt.ucReturn.err).
				Times(1)
			h := web.NewGetOrderHandler(uc)
			router := gin.Default()
			router.GET("/orders/:id", h.HandleGetOrder)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/orders/%s", tt.id), nil)

			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Code)
			assert.JSONEq(t, tt.wantResponseBody, w.Body.String())
		})
	}
}
