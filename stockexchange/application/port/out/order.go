//go:generate mockgen -destination=order_mock.go -package=out -source=order.go

package out

import (
	"context"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type UpsertOrderPort interface {
	InsertOrder(context.Context, domain.Order) error
	UpdateAllOrders(context.Context, ...domain.Order) error
}

type GetOrderPort interface {
	GetOrderByID(ctx context.Context, id string) (domain.Order, error)
}
