package service

import (
	"context"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/out"
)

type PlaceOrderService struct {
	generateOrderID     func() string
	orderPort           out.UpsertOrderPort
	orderBookPort       out.GetOrderBookPort
	reportExecutionPort out.ReportExecutionPort
}

func NewPlaceOrderService(
	generateOrderID func() string,
	orderPort out.UpsertOrderPort,
	orderBookPort out.GetOrderBookPort,
	reportExecutionPort out.ReportExecutionPort,
) *PlaceOrderService {
	return &PlaceOrderService{
		generateOrderID:     generateOrderID,
		orderPort:           orderPort,
		orderBookPort:       orderBookPort,
		reportExecutionPort: reportExecutionPort,
	}
}

// PlaceOrder accepts an Order and triggers order matching. Placing orders is async in
// nature, hence even if the order gets filled, the execution report will be sent via
// a separate channel rather than in the immediate response.
func (s *PlaceOrderService) PlaceOrder(
	ctx context.Context,
	symbol string,
	quantity uint,
	priceCent uint64,
	side string,
) (string, error) {
	id := s.generateOrderID()
	order, err := domain.NewOrder(id, symbol, quantity, priceCent, side, string(domain.StatusNew))
	if err != nil {
		return "", err
	}
	if err := s.orderPort.InsertOrder(ctx, order); err != nil {
		return "", err
	}
	orderBook, err := s.orderBookPort.GetOrderBook(ctx)
	if err != nil {
		return "", err
	}
	filledOrders := orderBook.Match(order)
	if len(filledOrders) > 0 {
		if err := s.orderPort.UpdateAllOrders(ctx, filledOrders...); err != nil {
			return "", err
		}
		s.reportExecutionPort.ReportExecution(ctx, filledOrders...)
	}
	return id, nil
}
